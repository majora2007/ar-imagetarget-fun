﻿using UnityEngine;
using System.Collections;

public class MyLifetimeBehaviour : MonoBehaviour {
	
	// Lifetime of the object. Once spawned, this will be cleaned up after Lifetime seconds.
	public float Lifetime = 10;
	
	// Use this for initialization
	void Start () {
		
		if (Lifetime < 0) Debug.LogError("Lifetime cannot be negative.");
		
		Destroy (gameObject, Lifetime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
