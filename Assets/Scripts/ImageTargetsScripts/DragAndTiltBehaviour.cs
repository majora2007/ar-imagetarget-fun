﻿using UnityEngine;
using System.Collections;

public class DragAndTiltBehaviour : MonoBehaviour {
	
	private float maxPickingDistance = 2000; // increase if needed, depending on your screen size
	private Transform pickedObject = null;
	
	private Vector3 rotationAxis = new Vector3(0f, 0f, 1f);
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		foreach(Touch touch in Input.touches)
		{
			Debug.Log ("Touching at: " + touch.position);
			Debug.Log ("Touch count: " + Input.touchCount);
			
			// Get the ray at position where the screen is touched
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			
			if (touch.phase == TouchPhase.Began)
			{
				Debug.Log ("Touch phase began at: " + touch.position);
				
				RaycastHit hit = new RaycastHit();
				if (Physics.Raycast(ray, out hit, maxPickingDistance))
				{
					pickedObject = hit.transform;
					Debug.Log ("Picked Object's Position: " + pickedObject.transform.position);
				} 
				else {
					pickedObject = null;
				}
			} else if (touch.phase == TouchPhase.Moved)
			{
				Debug.Log ("Touch phase Moved");
				
				if (pickedObject != null)
				{
					if (Input.touchCount == 1)
					{
						Vector2 screenDelta = touch.deltaPosition;
						
						float halfScreenWidth = 0.5f * Screen.width;
						float halfScreenHeight = 0.5f * Screen.height;
						
						float dx = screenDelta.x / halfScreenWidth;
						float dy = screenDelta.y / halfScreenHeight;
						
						Vector3 objectToCamera = pickedObject.transform.position - Camera.main.transform.position;
						float distance = objectToCamera.magnitude;
						
						float fovRad = Camera.main.fieldOfView * Mathf.Deg2Rad;
						float motionScale = distance * Mathf.Tan (fovRad/2);
						
						Vector3 translationInCameraRef = new Vector3(motionScale * dx, motionScale * dy, 0);
						
						Vector3 translationInWorldRef = Camera.main.transform.TransformDirection(translationInCameraRef);
						
						pickedObject.position += translationInWorldRef;
					}
					else if (Input.touchCount == 2) // Note: Unity is finicky about multi-touch. This does not work.
					{
						Vector2 screenDelta = touch.deltaPosition;
						
						float halfScreenHeight = 0.5f * Screen.height;
						float dy = screenDelta.y / halfScreenHeight;
						
						pickedObject.Rotate(rotationAxis, dy * Time.deltaTime, Space.World);
					}
				}
			} else if (touch.phase == TouchPhase.Ended)
			{
				Debug.Log ("Touch phase Ended");
				
				pickedObject = null;
			}
		}
	}
}
