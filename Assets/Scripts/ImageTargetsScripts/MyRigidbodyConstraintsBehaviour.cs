﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MyRigidbodyConstraintsBehaviour : MonoBehaviour {
	
	public float minY, maxY = 0f;
	
	private Vector3 mPosition;
	
	// Use this for initialization
	void Start () {
		mPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mPosition.y <= minY) mPosition.y = minY;
		else if (mPosition.y >= maxY) mPosition.y = maxY;
	
	}
}
