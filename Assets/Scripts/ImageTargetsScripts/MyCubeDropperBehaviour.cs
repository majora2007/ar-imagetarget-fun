﻿using UnityEngine;
using System.Collections;

public class MyCubeDropperBehaviour : MonoBehaviour {
	
	public Transform cubePrefab = null;
	
	private Camera mCamera;
	
	// Use this for initialization
	void Start () {
		if (cubePrefab == null) Debug.LogError("Cube Prefab must be assigned. Please assign one in the Editor.");
		
		mCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		foreach(Touch touch in Input.touches)
		{
			Debug.Log ("MyCubeDropperBehaviour - Touching at: " + touch.position);
			
			// Get the ray at position where the screen is touched
			Vector3 worldPoint = mCamera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 0f));
			
			if (touch.phase == TouchPhase.Began)
			{
				Debug.Log ("MyCubeDropperBehaviour - Touch phase began at: " + touch.position);
				
				// Spawn a new Cube (with a random z offset between -1 and 1)
				if (cubePrefab != null) 
				{
					float zOffset = Random.Range(-10f, 10f);
					if (worldPoint.y  == 0) worldPoint.y = 400;
					worldPoint.z += zOffset;
					Debug.Log ("MyCubeDropperBehaviour - Instantiating cube prefab at " + worldPoint);
					Instantiate(cubePrefab, worldPoint, Quaternion.identity);
				}
			}
		}
	
	}
}
