﻿using UnityEngine;
using System.Collections;

public class MyRotateBehaviour : MonoBehaviour {
	
	public float RotateSpeed = 10.0f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up, RotateSpeed * Time.deltaTime, Space.World);
	}
}
